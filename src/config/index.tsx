import { CSSProperties, defineComponent, VNodeChild } from "vue";
import {
  createRouter,
  createWebHistory,
  NavigationGuardWithThis,
  RouteComponent,
  RouteLocationNormalized,
  Router,
  RouteRecordRaw,
  RouteRecordRedirectOption,
} from "vue-router";
import { createPinia, Pinia } from "pinia";
import { GlobalThemeOverrides, zhCN, enUS, dateZhCN, dateEnUS, darkTheme, MenuOption, MenuProps } from "naive-ui";

export type LayoutRouteMeta = Record<string, { [key: string]: any }>;

export interface LayoutRouteIcon {
  name: string;
  color?: string;
  activeColor?: string;
}

export interface LayoutRoute {
  title: string;
  path: string;
  disabled?: boolean;
  hidden?: boolean;
  sort?: number;
  permission?: string;
  permissions?: { name: string; code: string }[];
  icon?: (() => VNodeChild) | LayoutRouteIcon;
  redirect?: RouteRecordRedirectOption;
  props?: boolean | Record<string, any> | ((to: RouteLocationNormalized) => Record<string, any>);
  alias?: string | string[];
  component?: RouteComponent | (() => Promise<RouteComponent>);
  children?: LayoutRoute[];
  beforeEnter?: NavigationGuardWithThis<undefined> | NavigationGuardWithThis<undefined>[];
}

export type Route = LayoutRoute;

export interface NaiveAdminPlugins {
  [name: string]: any;
  router: Router;
  store: Pinia;
}

export interface NaiveAdminRoutes {
  commonRoutes?: Record<string | 401 | 403 | 404, RouteRecordRaw & { meta: { title: string } }>;
  layoutRoutes?: LayoutRouteMeta | LayoutRoute[];
}

export interface NaiveAdminTheme {
  theme?: "light" | "dark";
  themeEditor?: boolean;
  lightThemeOverrides?: GlobalThemeOverrides;
  darkThemeOverrides?: GlobalThemeOverrides;
}

export interface NaiveAdminCache {
  tokenKey?: string;
  tagPagesKey?: string;
}

export interface NaiveAdminLayout {
  logoBar?: NaiveAdminLayoutLogoBar;
  siderBar?: NaiveAdminLayoutSiderBar;
  headerBar?: NaiveAdminLayoutHeaderBar;
  tagBar?: false | NaiveAdminLayoutTagBar;
  content?: NaiveAdminLayoutContent;
  page?: NaiveAdminLayoutPage;
  render?: NaiveAdminLayoutRender;
}

export interface NaiveAdminLayoutRender {
  logoBarRender?: () => VNodeChild;
  headerBarRender?: () => VNodeChild;
  headerBarLeftRender?: () => VNodeChild;
  headerBarRightRender?: () => VNodeChild;
  tagBarRender?: () => VNodeChild;
}

export interface NaiveAdminLayoutLogoBar {
  style?: CSSProperties;
  hiddenLogo?: boolean;
  hiddenTitle?: boolean;
}

export interface NaiveAdminLayoutSiderBar {
  style?: CSSProperties;
  width?: string | number;
  bordered?: boolean;
  menu?: NaiveAdminLayoutMenu;
}

export interface NaiveAdminLayoutHeaderBar {
  style?: CSSProperties;
  height?: string;
  collapseColor?: string;
  hiddenSiderCollapse?: boolean;
  hiddenBreadcrumb?: boolean;
}

export interface NaiveAdminLayoutTagBar {
  height?: string;
}

export interface NaiveAdminLayoutContent {
  style?: CSSProperties;
}

export interface NaiveAdminLayoutPage {
  padding?: string;
  gutter?: string;
  gap?: string;
}

export interface NaiveAdminLayoutMenu extends MenuProps {}

export interface INaiveAdminConfig {
  version?: string;
  title?: string;
  lang?: "zhCN" | "enUS";
  logo?: string | [string, string];
  collapsedLogo?: string;
  theme?: NaiveAdminTheme;
  cache?: NaiveAdminCache;
  layout?: NaiveAdminLayout;
  plugins?: NaiveAdminPlugins;
  routes?: NaiveAdminRoutes;
  permission?: (permissionRoutes: LayoutRoute[]) => string[] | Promise<string[]>;
}

export class NaiveAdminConfig implements INaiveAdminConfig {
  version: string = import.meta.env.VITE_VERSION ? String(import.meta.env.VITE_VERSION) : "";

  title: string = "";

  lang: "zhCN" | "enUS" = "zhCN";

  logo: string | [string, string] = "";

  theme: NaiveAdminTheme = {
    theme: "light",
    themeEditor: false,
    lightThemeOverrides: {},
    darkThemeOverrides: {},
  };

  cache: NaiveAdminCache = {
    tokenKey: "token",
    tagPagesKey: "tag_pages",
  };

  layout: NaiveAdminLayout = {
    logoBar: {
      style: {},
      hiddenLogo: false,
      hiddenTitle: false,
    },
    siderBar: {
      style: {},
      width: "272px",
      bordered: true,
      menu: {
        accordion: false,
      },
    },
    headerBar: {
      style: {},
      height: "64px",
      collapseColor: "rgba(0, 0, 0, 1)",
      hiddenSiderCollapse: false,
      hiddenBreadcrumb: false,
    },
    tagBar: {
      height: "34px",
    },
    content: {
      style: {
        backgroundColor: "rgba(245, 247, 249, 1)",
      },
    },
    page: {
      padding: "4px",
      gap: "0px",
      gutter: "4px",
    },
    render: {},
  };

  plugins: NaiveAdminPlugins = {
    router: createRouter({
      history: createWebHistory(),
      routes: [],
      strict: true,
      scrollBehavior: () => ({ left: 0, top: 0 }),
    }),
    store: createPinia(),
  };

  routes: NaiveAdminRoutes = {
    commonRoutes: {
      401: {
        path: "/login",
        component: defineComponent({ render: () => <div>No login component specified.</div> }),
        meta: { title: "登录" },
      },
      403: {
        path: "/403",
        component: defineComponent({ render: () => <div>No 403 component specified.</div> }),
        meta: { title: "403" },
      },
      404: {
        path: "/404",
        component: defineComponent({ render: () => <div>No 404 component specified.</div> }),
        meta: { title: "404" },
      },
    },
    layoutRoutes: [],
  };

  permission: (permissionRoutes: LayoutRoute[]) => string[] | Promise<string[]> = () => [];

  _layoutVueRoutes: RouteRecordRaw[] = [];

  _menus: MenuOption[] = [];

  get _locale() {
    if (this.lang === "zhCN") return zhCN;
    if (this.lang === "enUS") return enUS;
    return zhCN;
  }

  get _dateLocale() {
    if (this.lang === "enUS") return dateEnUS;
    return dateZhCN;
  }

  get _theme() {
    if (this.theme.theme === "light") return null;
    if (this.theme.theme === "dark") return darkTheme;
    return null;
  }

  get _themeOverrides() {
    if (this.theme.theme === "light") return this.theme.lightThemeOverrides;
    if (this.theme.theme === "dark") return this.theme.darkThemeOverrides;
    return this.theme.lightThemeOverrides;
  }

  get _firstMenu() {
    return this._menus[0].children ? this._menus[0].children[0] : this._menus[0];
  }
}
