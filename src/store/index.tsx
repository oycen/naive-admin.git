import { watch } from "vue";
import { defineStore } from "pinia";
import { RouteLocationNormalizedLoaded } from "vue-router";
import screenfull from "screenfull";
import { __APP_CACHE__, __APP_CONFIG__ } from "../app";

export const useApp = defineStore("NAIVE_ADMIN", {
  state: () => ({
    config: __APP_CONFIG__,
    menuCollapsed: false,
    token: __APP_CACHE__.token,
    user: null as unknown | null,
    permissionCodes: null as string[] | null,
    isAppFullscreen: screenfull.isFullscreen,
    tagPages: __APP_CACHE__.tagPages ?? [],
    pageViewKey: Date.now(),
  }),
  actions: {
    async signIn(token: string, user?: unknown) {
      try {
        this.token = token;
        this.user = user;
        this.config.plugins.router.replace("/");
      } catch (error) {
        throw error;
      }
    },
    async signOut() {
      try {
        this.token = null;
        this.user = null;
        this.config.plugins.router.replace({ name: "401" });
      } catch (error) {
        throw error;
      }
    },
    hasPermission(code: string) {
      if (!this.permissionCodes) return false;
      return this.permissionCodes.includes(code);
    },
    async toggleAppFullScreen() {
      await screenfull.toggle();
      this.isAppFullscreen = screenfull.isFullscreen;
      return this.isAppFullscreen;
    },
    async toggleContentFullScreen() {
      const ele = document.getElementById("layoutContent");
      if (ele) await screenfull.request(ele);
      return this.isAppFullscreen;
    },
    pushTagPage(route: RouteLocationNormalizedLoaded) {
      if (!route.meta.menuPath) return;
      if (this.tagPages.find((tag) => tag.name === route.name)) return;
      this.tagPages.push({ name: route.name as string, title: route.meta.title as string });
    },
    refreshPage() {
      this.pageViewKey = Date.now();
    },
    closeCurrentPage(route: RouteLocationNormalizedLoaded) {
      if (this.tagPages.length === 1) {
        window.$app.message.warning("至少保留一个标签页");
        return;
      }
      const index = this.tagPages.findIndex((tag) => tag.name === route.name);
      this.tagPages.splice(index, 1);
      return this.config.plugins.router.push({
        name: (this.tagPages[index - 1]?.name ?? this.tagPages[index]?.name) as string,
      });
    },
    closeOtherPages(route: RouteLocationNormalizedLoaded) {
      this.tagPages = [{ name: route.name as string, title: route.meta.title as string }];
    },
    closeAllPages(route: RouteLocationNormalizedLoaded) {
      if (route.name === this.config._firstMenu.name) {
        this.tagPages = [{ name: route.name as string, title: route.meta.title as string }];
      } else {
        this.tagPages = [];
        this.config.plugins.router.push("/");
      }
    },
  },
});

export function cacheStore() {
  const app = useApp();
  const cacheKeys = ["token", "tagPages"] as const;

  cacheKeys.forEach((key) => {
    watch(
      () => app.$state,
      (val: any) => (__APP_CACHE__[key] = val[key]),
      { deep: true }
    );
  });
}
