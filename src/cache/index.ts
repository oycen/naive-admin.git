import Cookies from "js-cookie";

export function createCache({
  tokenKey = "token",
  tagPagesKey = "tag_pages",
}: {
  tokenKey?: string;
  tagPagesKey?: string;
}) {
  return {
    get token() {
      return Cookies.get(tokenKey) ?? null;
    },
    set token(val: string | null) {
      val ? Cookies.set(tokenKey, val) : Cookies.remove(tokenKey);
    },
    get tagPages() {
      const tags = localStorage.getItem(tagPagesKey);
      return tags ? (JSON.parse(tags) as { name: string; title: string }[]) : null;
    },
    set tagPages(val: { name: string; title: string }[] | null) {
      val ? localStorage.setItem(tagPagesKey, JSON.stringify(val)) : localStorage.removeItem(tagPagesKey);
    },
  };
}
