import { ref } from "vue";
import { PageInst } from "../components/Page";

export function usePage() {
  const page = ref<PageInst>({} as PageInst);

  return page;
}
