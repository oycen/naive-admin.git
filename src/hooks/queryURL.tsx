import { reactive, watch as vWatch } from "vue";
import { useRouter, useRoute, LocationQueryValue } from "vue-router";

export function useQueryURL() {
  const router = useRouter();
  const route = useRoute();

  const methods = {
    setQuery: (query: object, watch: boolean = false) => {
      if (!watch) {
        router.replace({ query: Object.assign({}, route.query, query) });
        return;
      }

      vWatch(
        () => query,
        () => router.replace({ query: Object.assign({}, route.query, query) }),
        { deep: true, immediate: true }
      );
    },
    getQuery: <T extends string>(...properties: T[]) => {
      if (!properties.length) return route.query as Record<T, LocationQueryValue | LocationQueryValue[]>;

      return Object.fromEntries(properties.map((property) => [property, route.query[property]])) as Record<
        T,
        LocationQueryValue | LocationQueryValue[]
      >;
    },
    defineQuery: <T extends object>(
      target: T,
      format?: Partial<Record<keyof T, (value: LocationQueryValue | LocationQueryValue[], rawValue: any) => any>>
    ) => {
      const query = methods.getQuery();

      (Object.keys(target) as (keyof T)[]).forEach((key) => {
        const callback = format?.[key];
        const value = callback ? callback(query[key], target[key]) : defaultFormat(query[key], target[key]);
        if (value !== undefined || value !== null) target[key] = value;
      });

      const data = reactive(target);
      methods.setQuery(data, true);

      return data;
    },
  };

  return { ...methods };
}

function defaultFormat(value: LocationQueryValue | LocationQueryValue[], rawValue: any) {
  if (!value) return;
  if (typeof rawValue === "string" && typeof value === "string") return value;
  if (typeof rawValue === "number" && !isNaN(Number(value))) return Number(value);
  if (typeof rawValue === "boolean" && (value === "true" || value === "false")) return JSON.parse(value) as boolean;

  if (Array.isArray(rawValue) && Array.isArray(value)) {
    if (!rawValue.length) return;
    if (rawValue.every((value) => typeof value === "string") && value.every((value) => typeof value === "string")) {
      return value as string[];
    }
    if (rawValue.every((value) => typeof value === "number") && value.every((value) => !isNaN(Number(value)))) {
      return value.map(Number);
    }
    if (
      rawValue.every((value) => typeof value === "boolean") &&
      value.every((value) => value === "true" || value === "false")
    ) {
      return value.map((value) => JSON.parse(value as string) as boolean);
    }
  }
}
