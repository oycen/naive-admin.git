import { defineAsyncComponent } from "vue";
import { MenuOption } from "naive-ui";
import { RouteRecordRaw } from "vue-router";
import tree from "tree-array";
import { LayoutRouteIcon, Route } from "../config";
import clonedeep from "lodash.clonedeep";
import SvgIcon from "../components/SvgIcon";

export function resolveMetaGlobRoutes(meta: Record<string, { [key: string]: any }>): Route[] {
  return Object.keys(meta).flatMap((key) => {
    const module = meta[key].default || {};
    const modules = Array.isArray(module) ? [...module] : [module];
    return modules;
  });
}

export function resolveToVueRoutes(appRoutes: Route[]): RouteRecordRaw[] {
  const routes: any[] = [];

  tree(clonedeep(appRoutes)).forEach((node, { nodePath }) => {
    if (!node.component) return;

    let route: any = {};

    if (nodePath.some((node) => node.path.includes("/"))) {
      throw new Error(`The route path cannot contain '/', please use 'children' to divide the hierarchy.`);
    }

    route.component = node.component;
    route.name = nodePath.map(({ path }) => path.toLocaleUpperCase()).join("__");
    route.path = nodePath.map(({ path }) => path.toLocaleLowerCase()).join("/");
    node.redirect && (route.redirect = node.redirect);
    node.props && (route.props = node.props);
    node.alias && (route.alias = node.alias);
    node.beforeEnter && (route.beforeEnter = node.beforeEnter);

    route.meta = {};
    route.meta.title = node.title;
    route.meta.url = nodePath.map((node) => node.path).join("/");
    node.icon && (route.meta.icon = node.icon);
    node.disabled && (route.meta.disabled = node.disabled);
    node.hidden && (route.meta.hidden = node.hidden);
    node.sort && (route.meta.sort = node.sort);
    node.permission && (route.meta.permission = node.permission);
    node.permissions && (route.meta.permissions = node.permissions);
    route.meta.menuPath = nodePath.map((node, index) => {
      const name = nodePath.map(({ path }) => path).slice(0, index + 1);
      return {
        title: node.title,
        key: name.map((it) => it.toLocaleUpperCase()).join("__"),
        isDir: !node.component,
      };
    });

    routes.push(route);
  });

  return routes;
}

export function resolveToMenus(appRoutes: Route[]): MenuOption[] {
  const menus = tree(tree(appRoutes).filter((route) => !route.hidden)).map((route, { nodePath }) => {
    const name = nodePath.map(({ path }) => path.toLocaleUpperCase()).join("__");
    return {
      name,
      key: name,
      label: route.title,
      disabled: route.disabled,
      sort: route.sort,
      children: route.component ? undefined : route.children,
      iconRaw: route.icon,
    };
  });

  menus.sort((a, b) => (a?.sort ?? 0) - (b?.sort ?? 0));

  return menus as MenuOption[];
}
