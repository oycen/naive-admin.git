import { createApp as createVueApp, defineComponent, App as VueApp } from "vue";
import merge from "lodash.merge";
import { LayoutRoute, LayoutRouteMeta, INaiveAdminConfig, NaiveAdminConfig } from "../config";
import guard from "../guard";
import App from "../components/App";
import Layout from "../components/Layout";
import Page from "../components/Page";
import SvgIcon from "../components/SvgIcon";
import { cacheStore } from "../store";
import { resolveMetaGlobRoutes, resolveToMenus, resolveToVueRoutes } from "../utils";
import { notification } from "../notification";
import { createCache } from "../cache";

export let __APP_CONFIG__: NaiveAdminConfig;
export let __APP_CACHE__: ReturnType<typeof createCache>;

class NaiveAdmin {
  appConfig: NaiveAdminConfig;
  app: VueApp<Element>;

  constructor(config: NaiveAdminConfig) {
    __APP_CONFIG__ = config;
    __APP_CACHE__ = createCache({ tokenKey: config.cache.tokenKey, tagPagesKey: config.cache.tagPagesKey });
    this.appConfig = config;
    this.app = this.createApp();
    this.addCommonRoutes();
    this.addLayoutRoutes();
    this.usePlugins();
    this.routeGuard();
    this.cacheStore();
    this.registerComponents();
    this.notification();
  }

  createApp() {
    return createVueApp(defineComponent({ render: () => <App></App> }));
  }

  addCommonRoutes() {
    if (!this.appConfig.routes.commonRoutes) return;
    Object.entries(this.appConfig.routes.commonRoutes).forEach(([name, route]) =>
      this.appConfig.plugins.router.addRoute({ ...route, name })
    );
  }

  addLayoutRoutes() {
    if (!this.appConfig.routes.layoutRoutes) {
      this.appConfig._layoutVueRoutes = [];
      this.appConfig._menus = resolveToMenus([]);
    } else if (isNaN(Number(Object.keys(this.appConfig.routes.layoutRoutes)[0]))) {
      const routes = resolveMetaGlobRoutes(this.appConfig.routes.layoutRoutes as LayoutRouteMeta);
      this.appConfig.routes.layoutRoutes = routes;
      this.appConfig._layoutVueRoutes = resolveToVueRoutes(routes);
      this.appConfig._menus = resolveToMenus(routes);
    } else {
      this.appConfig._layoutVueRoutes = resolveToVueRoutes(this.appConfig.routes.layoutRoutes as LayoutRoute[]);
      this.appConfig._menus = resolveToMenus(this.appConfig.routes.layoutRoutes as LayoutRoute[]);
    }

    this.appConfig.plugins.router.addRoute({
      name: "LAYOUT",
      path: "/",
      component: Layout,
      redirect: { name: (this.appConfig._firstMenu?.name ?? "NOT_FOUND") as string },
      children: this.appConfig._layoutVueRoutes,
      meta: { title: "布局" },
    });
  }

  usePlugins() {
    Object.values(this.appConfig.plugins).forEach((plugin) => this.app.use(plugin));
  }

  routeGuard() {
    guard.start();
  }

  cacheStore() {
    cacheStore();
  }

  registerComponents() {
    this.app.component("AppPage", Page);
    this.app.component("SvgIcon", SvgIcon);
  }

  notification() {
    if (this.appConfig.version) {
      const version = localStorage.getItem("version");
      if (version && version !== this.appConfig.version)
        notification(this.appConfig.title, { body: `已更新至版本${this.appConfig.version}` });
      localStorage.setItem("version", this.appConfig.version);
    } else {
      localStorage.removeItem("version");
    }
  }
}

export const defineNaiveAdminConfig = (config: INaiveAdminConfig) => {
  return merge(new NaiveAdminConfig(), config) as NaiveAdminConfig;
};

export function createNaiveAdmin(config: NaiveAdminConfig) {
  const naiveAdmin = new NaiveAdmin(config);
  return naiveAdmin.app;
}
