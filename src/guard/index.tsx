import { LayoutRoute } from "../config";
import { useApp } from "../store";

export function start() {
  const app = useApp();

  app.config.plugins.router.beforeEach(async (to) => {
    if (app.token) {
      if (to.meta.permission) {
        if (!app.permissionCodes) await app.config.permission(app.config.routes.layoutRoutes as LayoutRoute[]);
        if (!app.hasPermission(to.meta.permission as string)) return { name: "403" };
      }
    } else {
      if (to.matched[0].name === "LAYOUT") return { name: "401" };
    }
  });

  app.config.plugins.router.afterEach((to) => {
    if (to.meta.title) {
      document.title = `${to.meta.title} - ${app.config.title}`;
    } else {
      document.title = `${app.config.title}`;
    }
  });

  app.config.plugins.router.onError(() => {
    window.$app.loadingBar.error();
  });
}

export default { start };
