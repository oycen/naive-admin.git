import { MessageApi, DialogApi, LoadingBarApi } from "naive-ui";

declare global {
  interface Window {
    $app: {
      message: MessageApi;
      dialog: DialogApi;
      loadingBar: LoadingBarApi;
    };
  }
}
