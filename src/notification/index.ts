export async function notification(title: string, options?: NotificationOptions) {
  try {
    if (!window.Notification) return;

    if (Notification.permission === "denied") {
      console.warn("Notification permission denied.");
      return;
    }

    if (Notification.permission === "granted") {
      new Notification(title, options);
      return;
    }

    if (Notification.permission === "default") {
      const permission = await Notification.requestPermission();
      if (permission === "granted") new Notification(title, options);
    }
  } catch (error) {
    throw error;
  }
}
