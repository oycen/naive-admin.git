import "./styles/index.css";

export { defineNaiveAdminConfig, createNaiveAdmin } from "./app";

export * from "./components/Page";
export { default as Page } from "./components/Page";
export { default as SvgIcon } from "./components/SvgIcon";

export * from "./config";

export { usePage } from "./hooks/page";
export { useQueryURL } from "./hooks/queryURL";

export { notification } from "./notification";

export { useApp } from "./store";
