import { defineComponent, computed } from "vue";
import { string } from "vue-types";

const SvgIcon = defineComponent({
  props: {
    prefix: string().def("icon"),
    name: string().isRequired,
    color: string(),
  },
  setup(props) {
    const symbolId = computed(() => `#${props.prefix}-${props.name}`);

    return () => (
      <svg aria-hidden="true">
        <use xlinkHref={symbolId.value} fill={props.color} />
      </svg>
    );
  },
});

export default SvgIcon;
