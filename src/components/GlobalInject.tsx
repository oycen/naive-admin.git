import { defineComponent } from "vue";
import { useMessage, useDialog, useLoadingBar } from "naive-ui";

const GlobalInject = defineComponent({
  setup() {
    window.$app = {
      message: useMessage(),
      dialog: useDialog(),
      loadingBar: useLoadingBar(),
    };

    return () => null;
  },
});

export default GlobalInject;
