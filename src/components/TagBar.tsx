import { defineComponent, watch } from "vue";
import { NSpace, NButton, NDropdown, NIcon } from "naive-ui";
import { ChevronDown24Regular } from "@vicons/fluent";
import { useRoute, useRouter, RouterLink } from "vue-router";
import { useApp } from "../store";

const TagBar = defineComponent({
  setup() {
    const app = useApp();
    const route = useRoute();
    const router = useRouter();

    watch(
      route,
      () => {
        app.pushTagPage(route);
      },
      { immediate: true }
    );

    const routes = router.getRoutes();
    const routeNames = routes.map(({ name }) => name);
    app.tagPages = app.tagPages.filter((tag) => routeNames.includes(tag.name));

    return () => (
      <NSpace
        size={[12, 0]}
        style={{
          "box-sizing": "content-box",
          height: app.config.layout.tagBar && app.config.layout.tagBar!.height,
          padding: "8px",
        }}
        justify="space-between"
      >
        <NSpace size={[6, 0]}>
          {app.tagPages.map((tag) => (
            <RouterLink to={{ name: tag.name }} style={{ textDecoration: "none" }}>
              <NButton type={route.name === tag.name ? "primary" : "default"}>{tag.title}</NButton>
            </RouterLink>
          ))}
        </NSpace>
        <NSpace>
          <NDropdown
            options={[
              {
                key: 0,
                label: "全屏当前",
              },
              {
                key: 1,
                label: "刷新当前",
              },
              {
                key: 2,
                label: "关闭当前",
              },
              {
                key: 3,
                label: "关闭其他",
              },
              {
                key: 4,
                label: "关闭全部",
              },
            ]}
            onSelect={(key) => {
              if (key === 0) app.toggleContentFullScreen();
              if (key === 1) app.refreshPage();
              if (key === 2) app.closeCurrentPage(route);
              if (key === 3) app.closeOtherPages(route);
              if (key === 4) app.closeAllPages(route);
            }}
          >
            <NButton style={{ padding: "0px 10px" }}>
              {{
                icon: () => (
                  <NIcon>
                    <ChevronDown24Regular></ChevronDown24Regular>
                  </NIcon>
                ),
              }}
            </NButton>
          </NDropdown>
        </NSpace>
      </NSpace>
    );
  },
});

export default TagBar;
