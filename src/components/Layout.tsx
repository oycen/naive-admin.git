import { defineComponent, Transition } from "vue";
import { NLayout, NLayoutSider, NLayoutHeader, NLayoutContent, NSpace } from "naive-ui";
import { RouterView } from "vue-router";
import { useApp } from "../store";
import SiderMenu from "./SiderMenu";
import LogoBar from "./LogoBar";
import HeaderToolsBar from "./HeaderToolsBar";
import Breadcrumb from "./Breadcrumb";
import SiderCollapse from "./SiderCollapse";
import TagBar from "./TagBar";

const Layout = defineComponent({
  setup() {
    const app = useApp();

    return () => (
      <NLayout has-sider position="absolute" style="width: 100%;height: 100%;top: 0;left： 0;">
        <NLayoutSider
          bordered={app.config.layout.siderBar!.bordered}
          collapseMode="width"
          width={app.config.layout.siderBar!.width}
          collapsedWidth={app.config.layout.siderBar?.menu?.collapsedWidth}
          nativeScrollbar={false}
          collapsed={app.menuCollapsed}
          onCollapse={() => (app.menuCollapsed = true)}
          onExpand={() => (app.menuCollapsed = false)}
          style={app.config.layout.siderBar!.style}
        >
          <LogoBar></LogoBar>
          <SiderMenu></SiderMenu>
        </NLayoutSider>
        <NLayout style={{ background: "#f5f7f9" }}>
          <NLayoutHeader position="absolute">
            <NSpace
              style={{
                height: app.config.layout.headerBar!.height,
                padding: "0 20px",
                boxShadow: "0 1px 4px rgb(0 21 41 / 8%)",
                ...app.config.layout.headerBar!.style,
              }}
              size={0}
              justify="space-between"
              align="center"
            >
              <NSpace size={18} align="center">
                {app.config.layout.render?.headerBarLeftRender ? (
                  app.config.layout.render?.headerBarLeftRender()
                ) : (
                  <>
                    {!app.config.layout.headerBar?.hiddenSiderCollapse && <SiderCollapse></SiderCollapse>}
                    {!app.config.layout.headerBar?.hiddenBreadcrumb && <Breadcrumb></Breadcrumb>}
                  </>
                )}
              </NSpace>
              {app.config.layout.render?.headerBarRightRender ? (
                app.config.layout.render?.headerBarRightRender()
              ) : (
                <HeaderToolsBar></HeaderToolsBar>
              )}
            </NSpace>
            {app.config.layout.tagBar &&
              (app.config.layout.render?.tagBarRender ? app.config.layout.render?.tagBarRender() : <TagBar></TagBar>)}
          </NLayoutHeader>
          <NLayoutContent
            // @ts-ignore
            id="layoutContent"
            position="absolute"
            style={{
              top: app.config.layout.tagBar
                ? `calc(${app.config.layout.headerBar!.height} + ${app.config.layout.tagBar!.height} + 8px + 8px)`
                : `calc(${app.config.layout.headerBar!.height})`,
              ...app.config.layout.content!.style,
            }}
          >
            <RouterView key={app.pageViewKey}>
              {{
                default: ({ Component }: any) => (
                  <Transition name="fade-slide" mode="out-in">
                    {Component && <Component></Component>}
                  </Transition>
                ),
              }}
            </RouterView>
          </NLayoutContent>
        </NLayout>
      </NLayout>
    );
  },
});

export default Layout;
