import { defineComponent, reactive } from "vue";
import { NAvatar, NIcon, NMenu, NTooltip } from "naive-ui";
import { FullscreenExitOutlined, FullscreenOutlined } from "@vicons/antd";
import { useApp } from "../store";

const HeaderToolsBar = defineComponent({
  setup() {
    const app = useApp();

    const state = reactive({
      options: [
        {
          key: "fullscreen",
          label: () => (
            <NTooltip>
              {{
                default: () => "全屏",
                trigger: () => (
                  <NIcon
                    size="18"
                    // @ts-ignore
                    onClick={() => app.toggleAppFullScreen()}
                  >
                    <FullscreenExitOutlined v-show={app.isAppFullscreen}></FullscreenExitOutlined>
                    <FullscreenOutlined v-show={!app.isAppFullscreen}></FullscreenOutlined>
                  </NIcon>
                ),
              }}
            </NTooltip>
          ),
        },
        {
          key: "avatar",
          label: () => (
            <NAvatar class="header-avatar" style="color: #fff;backgroundColor: rgb(28, 174, 91);">
              印
            </NAvatar>
          ),
          children: [
            {
              key: "user",
              label: "个人中心",
            },
            {
              key: "logout",
              label: () => <span onClick={() => app.signOut()}>退出登录</span>,
            },
          ],
        },
      ],
    });

    return () => (
      <NMenu
        class="header-menu"
        mode="horizontal"
        indent={10}
        options={state.options}
        themeOverrides={{ color: "rgba(255, 255, 255, 1)" }}
      />
    );
  },
});

export default HeaderToolsBar;
