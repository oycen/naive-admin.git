import { defineComponent } from "vue";
import { NBreadcrumb, NBreadcrumbItem } from "naive-ui";
import { useRoute, useRouter } from "vue-router";

const Breadcrumb = defineComponent({
  setup() {
    const route = useRoute();
    const router = useRouter();

    return () => (
      <NBreadcrumb>
        {(route.meta as any).menuPath.map((menu: { title: string; key: string; isDir: boolean }) => {
          const cursor = menu.isDir ? "not-allowed" : "pointer";
          return (
            <NBreadcrumbItem>
              <span
                style={{ cursor }}
                onClick={() => {
                  if (!router.hasRoute(menu.key)) return;
                  router.push({ name: menu.key });
                }}
              >
                {menu.title}
              </span>
            </NBreadcrumbItem>
          );
        })}
      </NBreadcrumb>
    );
  },
});

export default Breadcrumb;
