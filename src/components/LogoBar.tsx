import { defineComponent } from "vue";
import { RouterLink } from "vue-router";
import { NElement, NEllipsis } from "naive-ui";
import { useApp } from "../store";

const LogoBar = defineComponent({
  setup() {
    const app = useApp();

    // preload
    if (Array.isArray(app.config.logo)) {
      new Image().src = app.config.logo[0];
      new Image().src = app.config.logo[1];
    }

    return () => (
      <NElement
        style={{
          "box-sizing": "border-box",
          width: "100%",
          height: app.config.layout.headerBar!.height,
          display: "flex",
          "justify-content": "center",
          "align-items": "center",
          padding: "10px",
          ...app.config.layout.logoBar!.style,
        }}
      >
        {!app.config.layout.logoBar?.hiddenLogo && (
          <img
            style={{
              height: "80%",
              maxWidth: app.menuCollapsed ? "100%" : "",
              marginRight: app.menuCollapsed ? "" : "16px",
            }}
            src={
              typeof app.config.logo === "string"
                ? app.config.logo
                : app.menuCollapsed
                ? app.config.logo[1]
                : app.config.logo[0]
            }
            alt=""
          />
        )}
        {!app.config.layout.logoBar?.hiddenTitle && (
          <NEllipsis v-show={!app.menuCollapsed} tooltip={false}>
            <RouterLink
              style={{
                color: "#000",
                fontSize: "20px",
                fontWeight: "bold",
                "text-decoration": "none",
              }}
              to={{ path: "/" }}
            >
              {app.config.title}
            </RouterLink>
          </NEllipsis>
        )}
      </NElement>
    );
  },
});

export default LogoBar;
