import { defineComponent, computed, defineAsyncComponent } from "vue";
import { useRoute, RouterLink } from "vue-router";
import { NMenu, NIcon, NEllipsis } from "naive-ui";
import { useApp } from "../store";
import SvgIcon from "./SvgIcon";
import { LayoutRoute, LayoutRouteIcon } from "../config";

const SiderMenu = defineComponent({
  setup() {
    const route = useRoute();
    const app = useApp();

    const activeMenuKey = computed(() => {
      const menuPath = route.meta.menuPath as any[];
      const menu = menuPath.find((menu) => !menu.isDir);
      return menu.key;
    });

    const renderLabel = ({ name, label, children, disabled }: any) => {
      return children || disabled ? (
        <NEllipsis>{label}</NEllipsis>
      ) : (
        <RouterLink to={{ name }}>
          <NEllipsis>{label}</NEllipsis>
        </RouterLink>
      );
    };

    const renderIcon = ({ key, iconRaw }: { key: string; iconRaw: LayoutRoute["icon"] }) => {
      if (!iconRaw) return "";

      if (typeof iconRaw === "function") {
        return <NIcon>{iconRaw()}</NIcon>;
      }

      if (typeof iconRaw === "object") {
        return (
          <NIcon color={activeMenuKey.value === key ? iconRaw.activeColor : iconRaw.color}>
            <SvgIcon name={iconRaw.name}></SvgIcon>
          </NIcon>
        );
      }
    };

    return () => (
      <NMenu
        {...((app.config.layout.siderBar?.menu as any) ?? {})}
        value={activeMenuKey.value}
        options={app.config._menus as any[]}
        renderLabel={renderLabel}
        renderIcon={renderIcon}
      ></NMenu>
    );
  },
});

export default SiderMenu;
