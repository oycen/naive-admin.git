import { defineComponent } from "vue";
import { NIcon } from "naive-ui";
import { MenuUnfoldOutlined, MenuFoldOutlined } from "@vicons/antd";
import { useApp } from "../store";

const SiderCollapse = defineComponent({
  setup() {
    const app = useApp();

    return () => (
      <NIcon
        size="22"
        style="display: flex; align-items: center;cursor: pointer;"
        color={app.config.layout.headerBar?.collapseColor}
        // @ts-ignore
        onClick={() => (app.menuCollapsed = !app.menuCollapsed)}
      >
        <MenuUnfoldOutlined v-show={app.menuCollapsed}></MenuUnfoldOutlined>
        <MenuFoldOutlined v-show={!app.menuCollapsed}></MenuFoldOutlined>
      </NIcon>
    );
  },
});

export default SiderCollapse;
