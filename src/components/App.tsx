import { defineComponent, Transition } from "vue";
import { RouterView } from "vue-router";
import { NConfigProvider, NThemeEditor, NDialogProvider, NMessageProvider, NLoadingBarProvider } from "naive-ui";
import GlobalInject from "./GlobalInject";
import { useApp } from "../store";

const App = defineComponent({
  setup() {
    const app = useApp();

    const ThemeEditor: any = app.config.theme.themeEditor ? NThemeEditor : <div></div>;

    return () => (
      <NConfigProvider
        locale={app.config._locale}
        dateLocale={app.config._dateLocale}
        theme={app.config._theme}
        themeOverrides={app.config._themeOverrides}
      >
        <ThemeEditor>
          <NDialogProvider>
            <NMessageProvider>
              <NLoadingBarProvider>
                <GlobalInject />
                <RouterView>
                  {{
                    default: ({ Component }: any) => (
                      <Transition name="fade" mode="out-in">
                        {Component && <Component></Component>}
                      </Transition>
                    ),
                  }}
                </RouterView>
              </NLoadingBarProvider>
            </NMessageProvider>
          </NDialogProvider>
        </ThemeEditor>
      </NConfigProvider>
    );
  },
});

export default App;
