import { defineComponent, onMounted, reactive, CSSProperties } from "vue";
import { ModalProps, NModal, NScrollbar } from "naive-ui";
import { string, bool, array, object } from "vue-types";
import debounce from "lodash.debounce";
import { useApp } from "../store";

export interface PageModal {
  style(css?: CSSProperties): PageModal;
  props(props?: Omit<ModalProps, "show">): PageModal;
  open(): PageModal;
  close(): PageModal;
}

export interface PageInst {
  modal<T extends string>(modalName: T): PageModal;
}

const Page = defineComponent({
  props: {
    full: bool().def(false),
    padding: string(),
    areas: array<("laside" | "raside" | "header" | "main" | "footer")[]>().def([
      ["header", "header", "raside"],
      ["laside", "main", "raside"],
      ["footer", "footer", "footer"],
    ]),
    rows: string().def("auto 1fr auto"),
    cols: string().def("auto minmax(auto, 1fr) auto"),
    gap: string(),
    gutter: string(),
    slotStyle: object<{
      laside?: CSSProperties;
      raside?: CSSProperties;
      header?: CSSProperties;
      main?: CSSProperties;
      footer?: CSSProperties;
    }>(),
  },
  setup(props, ctx) {
    const app = useApp();

    const height = reactive({
      pageLeftAsideHeight: 0,
      pageRightAsideHeight: 0,
      pageHeaderHeight: 0,
      pageMainHeight: 0,
      pageFooterHeight: 0,
    });

    const modalsRaw = Object.fromEntries(
      Object.keys(ctx.slots)
        .filter((name) => name.indexOf("modal_") === 0)
        .map((name) => [
          name,
          { show: false, css: {}, props: {} } as { show: boolean; css: CSSProperties; props: Omit<ModalProps, "show"> },
        ])
    );
    const modals = reactive(modalsRaw);

    const getComputedHeight = (id: string) => {
      const ele = document.getElementById(id);
      if (!ele) return 0;

      const styleDeclaration = getComputedStyle(ele, null);

      return (
        parseFloat(styleDeclaration.getPropertyValue("height")) -
        parseFloat(styleDeclaration.getPropertyValue("padding-top")) -
        parseFloat(styleDeclaration.getPropertyValue("padding-bottom"))
      );
    };

    const setHeight = () => {
      height.pageLeftAsideHeight = getComputedHeight("page_left_aside");
      height.pageRightAsideHeight = getComputedHeight("page_right_aside");
      height.pageHeaderHeight = getComputedHeight("page_header");
      height.pageMainHeight = getComputedHeight("page_main");
      height.pageFooterHeight = getComputedHeight("page_footer");
    };

    const areasStyle = () => {
      return props.areas.map((it) => `"${it.join(" ")}"`).join(" ");
    };

    onMounted(() => {
      setHeight();
      window.addEventListener("resize", debounce(setHeight, 200));
    });

    const pageInst: PageInst = {
      modal(modalName) {
        const modal = modals["modal_" + modalName];
        if (!modal) throw new Error(`The '${modalName}' modal slot not delivered.`);
        return {
          style(css = {}) {
            modal.css = css;
            return this;
          },
          props(props = {}) {
            modal.props = props;
            return this;
          },
          open() {
            modal.show = true;
            return this;
          },
          close() {
            modal.show = false;
            return this;
          },
        };
      },
    };

    ctx.expose(pageInst);

    return () => (
      <section
        style={{
          "box-sizing": "border-box",
          height: "100%",
          display: "grid",
          padding: props.padding ?? app.config.layout.page!.padding,
          overflow: props.full ? "hidden" : "auto",
          gap: props.gap ?? app.config.layout.page!.gap,
          "grid-template-rows": props.rows,
          "grid-template-columns": props.cols,
          "grid-template-areas": areasStyle(),
        }}
      >
        {Object.entries(modals).map(([name, modal]) => {
          const slot = ctx.slots[name];
          return (
            slot && (
              <NModal
                v-model:show={modal.show}
                style={{
                  width: "600px",
                  "max-width": "80%",
                  ...modal.css,
                }}
                {...modal.props}
              >
                {slot()}
              </NModal>
            )
          );
        })}

        {ctx.slots.laside && (
          <aside
            id="page_left_aside"
            style={{
              "box-sizing": "border-box",
              "grid-area": "laside",
              margin: props.gutter ?? app.config.layout.page!.gutter,
              overflow: props.full ? "hidden" : "visible",
              ...props.slotStyle?.laside,
            }}
          >
            <NScrollbar style={{ "max-height": "100%" }}>
              {ctx.slots.laside({ height: height.pageLeftAsideHeight })}
            </NScrollbar>
          </aside>
        )}

        {ctx.slots.raside && (
          <aside
            id="page_right_aside"
            style={{
              "box-sizing": "border-box",
              "grid-area": "raside",
              margin: props.gutter ?? app.config.layout.page!.gutter,
              overflow: props.full ? "hidden" : "visible",
              ...props.slotStyle?.raside,
            }}
          >
            <NScrollbar style={{ "max-height": "100%" }}>
              {ctx.slots.raside({ height: height.pageRightAsideHeight })}
            </NScrollbar>
          </aside>
        )}

        {ctx.slots.header && (
          <header
            id="page_header"
            style={{
              "box-sizing": "border-box",
              "grid-area": "header",
              margin: props.gutter ?? app.config.layout.page!.gutter,
              display: "flex",
              "justify-content": "space-between",
              "align-items": "center",
              ...props.slotStyle?.header,
            }}
          >
            {ctx.slots.header({ height: height.pageHeaderHeight })}
          </header>
        )}

        {(ctx.slots.main || ctx.slots.default) && (
          <main
            id="page_main"
            style={{
              "box-sizing": "border-box",
              "grid-area": "main",
              margin: props.gutter ?? app.config.layout.page!.gutter,
              overflow: props.full ? "hidden" : "visible",
              ...props.slotStyle?.main,
            }}
          >
            <NScrollbar style={{ "max-height": "100%" }}>
              {ctx.slots.main && !ctx.slots.default && ctx.slots.main({ height: height.pageMainHeight })}
              {!ctx.slots.main && ctx.slots.default && ctx.slots.default({ height: height.pageMainHeight })}
            </NScrollbar>
          </main>
        )}

        {ctx.slots.footer && (
          <footer
            id="page_footer"
            style={{
              "box-sizing": "border-box",
              "grid-area": "footer",
              margin: props.gutter ?? app.config.layout.page!.gutter,
              display: "flex",
              "justify-content": "space-between",
              "align-items": "center",
              ...props.slotStyle?.footer,
            }}
          >
            {ctx.slots.footer({ height: height.pageFooterHeight })}
          </footer>
        )}
      </section>
    );
  },
});

export default Page;
