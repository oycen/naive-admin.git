import path from "path";
import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";
import vueJSX from "@vitejs/plugin-vue-jsx";

export default defineConfig({
  plugins: [vue(), vueJSX()],
  build: {
    cssCodeSplit: true,
    lib: {
      entry: path.resolve(__dirname, "src/index.ts"),
      name: "naiveAdmin",
      fileName: (format) => `naive-admin.${format}.js`,
    },
    rollupOptions: {
      external: ["naive-ui", "pinia", "vue", "vue-router", "vue-types"],
      output: {
        globals: {
          "naive-ui": "naiveUi",
          pinia: "pinia",
          vue: "vue",
          "vue-router": "vueRouter",
          "vue-types": "vueTypes",
        },
      },
    },
  },
});
